export ELAPSED_TIME="\e[90;47m%e\e[0m" # Unused?
export TERM="xterm-color"
export TIME=$'e:\e[90;47m%e\e[0m U:%U S:%S seconds I:%I O:%O M:%M'
export TIME4BUSYBOX='    I:%I    O:%O    M:%M'
export TIMEFORMAT=$'\e[90;47m%R\e[0m seconds'
#^ for bash time

echo ELAPSED_TIME
/usr/bin/printf '%q\n' "${ELAPSED_TIME}"
printf '%q\n' "${ELAPSED_TIME}"
echo "${ELAPSED_TIME@Q}" || true
echo TIME
/usr/bin/printf '%q\n' "${TIME}"
printf '%q\n' "${TIME}"
echo "${TIME@Q}" || true
echo TIME4BUSYBOX
/usr/bin/printf '%q\n' "${TIME4BUSYBOX}"
printf '%q\n' "${TIME4BUSYBOX}"
echo "${TIME4BUSYBOX@Q}" || true
echo TIMEFORMAT
/usr/bin/printf '%q\n' "${TIMEFORMAT}"
printf '%q\n' "${TIMEFORMAT}"
echo "${TIMEFORMAT@Q}" || true
