# time

Runs a program and displays information about the resources used https://www.gnu.org/software/time/

* [*`time` versus shell builtin*
  ](https://gitlab.com/software-packages-demo/time/-/blob/main/README.md#time-versus-shell-builtin)
  software-packages-demo/time
